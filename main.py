import re
import os
import tkinter as tk
from tkinter import filedialog
from tkinter import ttk
import warnings
import logging

log = logging.getLogger(__name__)
# TODO define an ALWAYS log level

logging.basicConfig(
    level=logging.INFO,
    # format="%(name)s: %(asctime)s | %(levelname)s | %(filename)s:%(lineno)s | %(process)d >>> %(message)s",
    format="%(asctime)s %(levelname)s\t> %(message)s",
    # datefmt="%Y-%m-%dT%H:%M:%SZ"
    datefmt="%H:%M:%S",
)


# global definitions
modPath = ''
# # script scope values, used to ckeck which condition is valid
# 	- global (outside any monitor)
# 	- monitorDefinition
# 	- monitorBody
# 	- blockDefinition (for if and while)
# 	- blockBody
# 	- spawnArmy
# 	- add_event
scriptScope = 'global'
# globals used to follow code structure
openedMonitor = 0
openedBlock = 0
openedSpawn = 0
openedAddEvent = 0


def resetGlobals():
	scriptScope = 'global'
	openedMonitor = 0
	openedBlock = 0
	openedSpawn = 0
	openedAddEvent = 0


def stripComments(line):
	'''	Remove commented lines, works wherever the semicolon is.'''
	if re.search(r'^;', line) is not None:
		line = ''
	if re.search(r';', line) is not None:
		line = line.split(';')[0]
	return line

def getModFolder():
	''' to be called from the GUI button, store mod folder as a global '''
	global modPath
	modPath = filedialog.askdirectory()
	log.info('Selected mod path : '+modPath)

def searchCampaigns():
	''' look for every campaign folder in /data/world/maps/campaign '''
	log.info('Listing all campaign folders')
	customFolder = modPath+'/data/world/maps/campaign/custom/'
	# TODO do better handling error, and in calling function. try/catch?
	if modPath == '': return
	names = os.listdir(customFolder)
	scriptsPath = []
	for name in names :
		campaignPath = customFolder+'/'+name
		if os.path.isdir(campaignPath): 
			scriptsPath.append(campaignPath+'/campaign_script.txt')
			log.info('\tFound campaign folder : '+name)
	return scriptsPath

# TODO list unterminated monitors
def followScope(line):
	''' update script following globals to follow code structure, and log error if blocks are not closed correctly '''
	global scriptScope
	match scriptScope:		
		case 'global' :
			match line[0]:
				case 'monitor_event':
					scriptScope = 'monitorDefinition'
					log.debug('\tmonitor declaration (line TODO)')
				case 'monitor_condition':
					scriptScope = 'monitorDefinition'
					log.debug('\tmonitor declaration (line TODO)')
				case 'end_monitor':
					scriptScope = 'monitorDefinition'
					log.debug('\tmonitor closed (line TODO)')
		case 'monitorDefinition':
			match line[0] :
				case 'and':
					return
				case 'if' :
					scriptScope = 'blockDefinition'
					log.debug('\tblock declaration (line TODO)')
				case _: 
					scriptScope = 'monitorBody'
					log.debug('\tmonitor body start (line TODO)')
		case 'monitorBody':
			return #TODO
		case 'blockDefinition':
			return #TODO
		case _:
			log.error('unrecognised scope at line TODO')
	print(scriptScope)

def parseCampaignScript(path):
	''' parse and tokenise campaign script and call all necessary checks '''
	with open(modPath+'/data/world/maps/campaign/imperial_campaign/campaign_script.txt') as cs:
		log.info('Parsing script : '+path)
		resetGlobals()
		log.warning('TEST '+scriptScope)
		for line in cs:
			line = stripComments(line).strip()
			# ignore empty lines and log lines
			if  line == '': continue
			# tokenize line
			tokens = [s.strip() for s in line.split() if s]
			# ignore log lines
			if tokens[0] == ('log' or 'log_counter'): continue
			followScope(tokens)


def runCheck():
	''' used by button in GUI to trigger main routine for every found campaign script '''
	if modPath == '': warnings.warn('WARNING : mod folder must be selected')
	# TODO automaticcaly find all campaign scripts and check all of them (use an array?)
	pathList = searchCampaigns()
	for path in pathList:
		parseCampaignScript(path)


# define GUI window
root = tk.Tk()
frm = ttk.Frame(root)
root.geometry('300x150')
frm.grid()
frm.master.title('syntax checker')
ttk.Button(frm, text="Select Mod Folder", command=getModFolder).grid(column=0, row=0)
ttk.Button(frm, text="Run Check", command=runCheck).grid(column=1, row=0)
ttk.Button(frm, text="Quit", command=root.destroy).grid(column=2, row=0)
root.mainloop()


