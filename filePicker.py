import tkinter as tk
from tkinter import filedialog
from tkinter import ttk

def getModFilder():
    modPath = filedialog.askdirectory()
    print(modPath)


root = tk.Tk()
frm = ttk.Frame(root)
root.geometry('300x150')
frm.grid()
frm.master.title('syntax checker')
ttk.Button(frm, text="Select Mod Folder", command=getModFilder).grid(column=0, row=0)
ttk.Button(frm, text="Run Check", command=runCheck).grid(column=1, row=0)
ttk.Button(frm, text="Quit", command=root.destroy).grid(column=2, row=0)
root.mainloop()

# file_path = filedialog.askopenfilename()
# new_file = input("Name file\n")
# open_file = open(f"{file_path}\%s.py" % new_file, 'w')



# import tkinter as tk
# from tkinter import ttk
# from tkinter import filedialog as fd
# from tkinter.messagebox import showinfo

# # create the root window
# root = tk.Tk()
# root.title('Tkinter Open Directory Dialog')
# root.resizable(False, False)
# root.geometry('300x150')


# def select_file():
#     filetypes = (
#         ('text files', '*.txt'),
#         ('All files', '*.*')
#     )

#     filename = fd.askopenfilename(
#         title='Open a file',
#         initialdir='/',
#         filetypes=filetypes)

#     showinfo(
#         title='Selected File',
#         message=filename
#     )


# # open button
# open_button = ttk.Button(
#     root,
#     text='Open a File',
#     command=select_file
# )

# open_button.pack(expand=True)


# # run the application
# root.mainloop()