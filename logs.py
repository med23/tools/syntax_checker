import logging

log = logging.getLogger(__name__)

logging.basicConfig(
    level=logging.INFO,
    # format="%(name)s: %(asctime)s | %(levelname)s | %(filename)s:%(lineno)s | %(process)d >>> %(message)s",
    format="%(name)s: %(asctime)s %(levelname)s : %(message)s",
    # datefmt="%Y-%m-%dT%H:%M:%SZ",
    datefmt="%H:%M",
)


log.debug("A debug message")
log.info("An info message")
log.warning("A warning message")
log.error("An error message")
log.critical("A critical message")